import torch
import torch.nn as nn
from torch import optim
import time, random
import os
from tqdm import tqdm
from lstm import LSTMSentiment
from bilstm import BiLSTMSentiment
from torchtext import data
import numpy as np
import argparse
import gensim
import logging
torch.set_num_threads(8)
torch.manual_seed(1)
random.seed(1)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
logger.info('1')

def load_bin_vec(word_embedding_path, text_field, dim):
    word_vecs = {}
    word2vec_model = gensim.models.KeyedVectors.load_word2vec_format(word_embedding_path, binary=False)
    for word in word2vec_model.index2word:
        if word in text_field.vocab.stoi:
            word_vecs[word] = word2vec_model.word_vec(word)

    text_field.vocab.vectors = torch.Tensor(len(text_field.vocab), dim)
    for i, token in enumerate(text_field.vocab.itos):
        if token in word_vecs:
            text_field.vocab.vectors[i] = torch.FloatTensor(word_vecs[token])
        else:
            # initialize <unk> with U(-0.25, 0.25) vectors
            text_field.vocab.vectors[i] = torch.FloatTensor(dim).uniform_(-0.25, 0.25)
    return word_vecs


def get_accuracy(truth, pred):
    assert len(truth) == len(pred)
    right = 0
    for i in range(len(truth)):
        if truth[i] == pred[i]:
            right += 1.0
    return right / len(truth)



def evaluate(model, data, loss_function, name):
    model.eval()
    avg_loss = 0.0
    truth_res = []
    pred_res = []
    for batch in data:
        sent, label = batch.text, batch.label
        label.data.sub_(1)
        truth_res += list(label.data)
        model.batch_size = len(label.data)
        model.hidden = model.init_hidden()
        label, sent = label.cuda(), sent.cuda()
        pred = model(sent)
        pred_label = pred.data.max(1)[1].cpu().numpy()
        pred_res += [x for x in pred_label]
        loss = loss_function(pred, label)
        avg_loss += loss.data[0]
    avg_loss /= len(data)
    acc = get_accuracy(truth_res, pred_res)
    print(name + ': loss %.2f acc %.1f' % (avg_loss, acc * 100))
    return acc


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--m', dest='model', default='lstm', help='specify the mode to use (default: lstm)')
    parser.add_argument('--dataset', type=str, default='bioprocess', help='sst or bioprocess')
    parser.add_argument('--words_dim', type=int, default=100, help="")
    parser.add_argument('--static_mode', type=str, default='static')
    parser.add_argument('--word_embedding_path', type=str, default='../../pretrained/pubmed.vec')
    parser.add_argument('--batch_size', type=int, default=64)
    parser.add_argument('--device', type=int, default=1, help='device to use for iterate data, -1 mean cpu [default: -1]')
    parser.add_argument('-test_interval', type=int, default=20, help='how many steps to wait before testing [default: 100]')
    parser.add_argument('-early_stop', type=int, default=100, help='iteration numbers to stop without performance increasing')
    parser.add_argument('-epochs', type=int, default=20, help='')
    config = parser.parse_args()

    USE_GPU = torch.cuda.is_available()
    HIDDEN_DIM = 150
    timestamp = str(int(time.time()))
    out_dir = os.path.abspath(os.path.join(os.path.curdir, "runs", timestamp))

    def load_sst(text_field, label_field, batch_size):
        train, dev, test = data.TabularDataset.splits(path='./data/SST2/', train='train.tsv',
                                                      validation='dev.tsv', test='test.tsv', format='tsv',
                                                      fields=[('text', text_field), ('label', label_field)])
        text_field.build_vocab(train, dev, test)
        label_field.build_vocab(train, dev, test)
        train_iter, dev_iter, test_iter = data.BucketIterator.splits((train, dev, test), batch_sizes=(batch_size, len(dev), len(test)), sort_key=lambda x: len(x.text), repeat=False, device=config.device)
        return train_iter, dev_iter, test_iter

    def load_bioprocess(text_field, label_field, batch_size):
        train, dev, test = data.TabularDataset.splits(path='./data/BioProcess/', train='train.tsv',
                                                      validation='dev.tsv', test='test.tsv', format='tsv',
                                                      fields=[('label', label_field), ('text', text_field)])
        text_field.build_vocab(train, dev, test)
        label_field.build_vocab(train, dev, test)
        train_iter, dev_iter, test_iter = data.BucketIterator.splits((train, dev, test), batch_sizes=(batch_size, len(dev), len(test)), sort_key=lambda x: len(x.text), repeat=False, device=config.device)
        return train_iter, dev_iter, test_iter

    text_field = data.Field(lower=True)
    label_field = data.Field(sequential=False)
    if config.dataset == 'sst':
        train_iter, dev_iter, test_iter = load_sst(text_field, label_field, config.batch_size)
    else:
        train_iter, dev_iter, test_iter = load_bioprocess(text_field, label_field, config.batch_size)

    model = BiLSTMSentiment(embedding_dim=config.words_dim, hidden_dim=HIDDEN_DIM, vocab_size=len(text_field.vocab), label_size=len(label_field.vocab) - 1,
                            use_gpu=USE_GPU, batch_size=config.batch_size, config=config)

    if config.device >= 0:
        torch.cuda.set_device(config.device)
        model = model.cuda()

    logger.info('Load word embeddings...')
    # # glove
    # text_field.vocab.load_vectors('glove.6B.100d')

    # word2vector
    load_bin_vec(config.word_embedding_path, text_field, config.words_dim)
    model.embeddings.weight.data.copy_(text_field.vocab.vectors)

    best_model = model
    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=1e-3)
    loss_function = nn.NLLLoss()

    best_dev_acc = 0.0
    steps = 0
    early_stop = False

    print('Training...')
    print("Writing to {}\n".format(out_dir))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for epoch in range(config.epochs):
        if early_stop:
            logger.info('early stop by {} steps.'.format(config.early_stop))
            break

        sum_loss = 0.0
        truth_res = []
        pred_res = []
        count = 0

        for batch in tqdm(train_iter, desc='Train epoch ' + str(epoch + 1)):
            model.train()
            sent, label = batch.text, batch.label
            label.data.sub_(1)
            truth_res += list(label.data)
            model.batch_size = len(label.data)
            model.hidden = model.init_hidden()
            label, sent = label.cuda(), sent.cuda()
            pred = model(sent)
            pred_label = pred.data.max(1)[1].cpu().numpy()
            pred_res += [x for x in pred_label]
            model.zero_grad()
            loss = loss_function(pred, label)
            sum_loss += loss.data[0]
            count += 1
            steps += 1
            loss.backward()
            optimizer.step()

            if steps % config.test_interval == 0:
                avg_loss = sum_loss / count
                acc = get_accuracy(truth_res, pred_res)
                tqdm.write('Train: loss %.2f acc %.1f' % (avg_loss, acc * 100))
                dev_acc = evaluate(model, dev_iter, loss_function, 'Dev')
                if dev_acc > best_dev_acc:
                    if best_dev_acc > 0:
                        os.system('rm ' + out_dir + '/best_model' + '.pth')
                    best_dev_acc = dev_acc
                    best_model = model
                    torch.save(best_model.state_dict(), out_dir + '/best_model' + '.pth')
                    # evaluate on test with the best dev performance model
                    evaluate(best_model, test_iter, loss_function, 'Test')
                    last_step = steps
                else:
                    if steps - last_step >= config.early_stop:
                        early_stop = True
                        break

    evaluate(best_model, test_iter, loss_function, 'Final Test')


if __name__ == '__main__':
    main()